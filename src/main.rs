use chumsky::prelude::Parser as chumskyparser;
use chumsky::prelude::{Simple, one_of, recursive, just, end};
use chumsky::text;
use std::io::{self, BufRead};
use std::fs::OpenOptions;



use std::io::BufReader;
use std::collections::HashSet;
use std::process::exit;
use std::fs::File;
use std::iter;

use std::fmt;
use std::io::Write;



use num_rational::Ratio;
use num_bigint::BigInt;

use std::collections::HashMap;
use std::collections::BTreeSet;

use priority_queue::PriorityQueue;



use clap::{Args, CommandFactory, FromArgMatches, Parser, Subcommand, arg, command};
#[derive(Parser)]
#[command(author, version)]
#[command(about = "", long_about = "")]

struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// Liest eine Sequenz Konstituentenbäume von der Standardeingabe und gibt eine aus diesen Bäumen induzierte PCFG auf der Standardausgabe aus. Wird das optionale Argument GRAMMAR angegeben , dann wird die PCFG stattdessen in den Dateien GRAMMAR .rules , GRAMMAR . lexicon und GRAMMAR .words gespeichert (s. (1)).
    Induce(Induce),
    Parse(Parse),
    Binarise(Binarise),
    Debinarise,
    Unk(Unk),
    Smooth,
    Outside,
}


#[derive(Args)]
struct Induce {

    grammar: Option<String>,
}

#[derive(Args)]
struct Parse {
    #[arg(short, long)]
    paradigma: Option<String>,
     #[arg(short, long="initial_nonterminal")]
    initial_nonterminal: Option<String>,
     #[arg(short, long="unking")]
    unking: bool,
     #[arg(short, long)]
    smoothing: bool,
    #[arg(short, long="threshold-beam")]
    threshold_beam: Option<String>,
    #[arg(short, long="rank-beam")]
    rank_beam: Option<String>,
    #[arg(short, long)]
    astar: Option<String>,


    rules: String,
    lexicon: String,
}

#[derive(Args)]
struct Unk {
    #[arg(short, long)]
    threshold: u32,
}

#[derive(Args)]
struct Binarise {
    #[arg(short, long)]
    horizontal: Option<u32>,
    #[arg(short, long)]
    vertical: Option<u32>,
}
















#[derive(Debug, Clone, PartialEq, Hash, Eq)]
enum Rule {
    NonLexical{lhs: String, rhs: Vec<Rule>},
    Lexical{lhs: String, rhs: String},
}




fn main() {
    let cli = Cli::parse();
    match &cli.command {
        Some(Commands::Induce(induce)) => {
            //let example = "(N John)";
            //let example = "(ROOT (S (NP-SBJ (NP (NNP Pierre) (NNP Vinken)) (, ,) (ADJP (NP (CD 61) (NNS years)) (JJ old)) (, ,)) (VP (MD will) (VP (VB join) (NP (DT the) (NN board)) (PP-CLR (IN as) (NP (DT a) (JJ nonexecutive) (NN director))) (NP-TMP (NNP Nov.) (CD 29)))) (. .)))";



            let mut rules_map: HashMap<(String,String), u32> = HashMap::new();
            let mut lexicon_map: HashMap<(String,String), u32>  = HashMap::new();
            let mut count_map: HashMap<String, u32> =HashMap::new();
            let mut all_words = HashSet::new();

            let stdin = io::stdin();
            let mut line_number = 0;
            for line in stdin.lock().lines() {
                line_number += 1;
                let example = line.unwrap();





                let parsed_ptb = ptb_parser().parse(example);

                match parsed_ptb {
                    Ok(rule_tree) => {
                        let lexicon;
                        let rules;
                        let words;
                        (rules, lexicon, words) = readoff_rule(&rule_tree);



                        for word in words {
                            all_words.insert(word);
                        }

                        for rule in rules {
                            *rules_map.entry(rule.to_owned()).or_default() +=1;
                            *count_map.entry(rule.0).or_default() +=1;
                        }
                        for rule in lexicon {
                            *lexicon_map.entry(rule.to_owned()).or_default() +=1;
                            *count_map.entry(rule.0).or_default() +=1;
                        }




                    },
                    Err(e) => {
                    eprintln!("Parsing Error on line {}: {}", line_number, e[0]);
                    exit(1);
                    },
                }



            }
            match &induce.grammar {
            Some(grammar) => {
                let mut rules_file = OpenOptions::new()
                    .create(true)
                    .write(true)
                    .append(true)
                    .open(format!("{grammar}.rules"))
                    .unwrap();
                for ((lhs, rhs),value) in rules_map {
                    let ratio = f64::from(value)/ f64::from(*count_map.get(&lhs).unwrap());
                    writeln!(rules_file, "{lhs} -> {rhs} {ratio}");
                }
                let mut lexicon_file = OpenOptions::new()
                    .create(true)
                    .write(true)
                    .append(true)
                    .open(format!("{grammar}.lexicon"))
                    .unwrap();
                for ((lhs, rhs),value) in lexicon_map {
                    let ratio = f64::from(value)/ f64::from(*count_map.get(&lhs).unwrap());
                    writeln!(lexicon_file, "{lhs} {rhs} {ratio}");
                }
                let mut words_file = OpenOptions::new()
                    .create(true)
                    .write(true)
                    .append(true)
                    .open(format!("{grammar}.words"))
                    .unwrap();
                for word in all_words {
                    writeln!(words_file, "{word}");
                }
            },
            None => {

                for ((lhs, rhs),value) in rules_map {
                    let ratio = f64::from(value)/ f64::from(*count_map.get(&lhs).unwrap());
                    println!("{lhs} -> {rhs} {ratio}")
                }

                for ((lhs, rhs),value) in lexicon_map {
                    let ratio = f64::from(value)/ f64::from(*count_map.get(&lhs).unwrap());
                    println!("{lhs} {rhs} {ratio}")
                }


                for word in all_words {
                println!("{word}");
                }
            },
            }


        },
        Some(Commands::Parse(parse)) => {
            let mut rules: BTreeSet<ParseRule> = BTreeSet::new();
            let mut lexicon: BTreeSet<ParseLexical> = BTreeSet::new();
            let mut all_words = HashSet::new();



            let rules_file = File::open(parse.rules.clone());
            let lexicon_file = File::open(parse.lexicon.clone());
            let unking = parse.unking;

            let rules_parser = rules_list_parser();

            for line in BufReader::new(rules_file.unwrap()).lines() {
                let parsed_rule = rules_parser.parse(line.unwrap());
                match parsed_rule {
                    Ok(rule) => {
                        rules.insert(rule);
                    },
                    Err(_e) => {},
                }
            }



            let lexical_parser = lexical_rule_parser();
            for line in BufReader::new(lexicon_file.unwrap()).lines() {
                let parsed_rule = lexical_parser.parse(line.unwrap());
                match parsed_rule {
                    Ok(rule) => {
                        all_words.insert(rule.rhs.clone());
                        lexicon.insert(rule);
                        
                    },
                    Err(_e) => {},
                }
            }

            let root;
            match &parse.initial_nonterminal {
                Some(result) => {
                    root = result.to_string();
                },
                _ => {
                    root = "ROOT".to_string();
                },
            }
            //println!("{:?}", all_words);

            let stdin = io::stdin();
            let _line_number = 0;
            for line in stdin.lock().lines() {
                let wordsResult = parse_sentence(line.unwrap());
                let WeightMap;
                match wordsResult {
                Ok(words) => {
                    let mut unked_words: Vec<String> = vec![];
                    if unking {
                        let mut new_sentence: Vec<String> = vec![];
                        
                        for word in words.clone() {
                            if let Some(w) = all_words.get(&word) {
                                new_sentence.push(word);
                            } else {
                                new_sentence.push("UNK".to_string());
                                unked_words.push(word);
                            }
                            //println!("{:?}", unked_words);

                        }
                        //println!("{:?}", new_sentence);
                        WeightMap = weightedCYK(lexicon.clone(), rules.clone(), new_sentence.clone());
                    } else {
                        WeightMap = weightedCYK(lexicon.clone(), rules.clone(), words.clone());
                    }

                    
                    //println!("{:?}", WeightMap);
                    let zero: usize = 0;
                    match WeightMap.get(&(zero,words.len(), root.clone())) {
                        Some((weight, rule)) => {
                            if weight > &Ratio::from_integer(BigInt::from(0)) {
                                if unking {
                                    let mut parsed_unked = format!("{rule}");
                                    for word in unked_words {                                      parsed_unked = parsed_unked.replacen("UNK", &word, 1);
                                    }
                                    println!("{}", parsed_unked);
                                } else {
                                    println!("{rule}");
                                }
                            } else {
                                println!("(NOPARSE {})", words.join(" "));
                            }
                        },
                        _ => {},
                    }


                },
                Err(_e) => {},
                }


            }


        },
        Some(Commands::Unk(unk)) => {

            let mut count_map: HashMap<String, u32> =HashMap::new();
            let mut input: Vec<Rule> = vec![];

            let stdin = io::stdin();
            let mut line_number = 0;
            for line in stdin.lock().lines() {
                line_number += 1;
                let example = line.unwrap();
                




                let parsed_ptb = ptb_parser().parse(example);

                match parsed_ptb {
                    Ok(rule_tree) => {
                        input.push(rule_tree.clone());
                        let words;
                        (_, _, words) = readoff_rule(&rule_tree);



                        for word in words {
                            *count_map.entry(word).or_default() +=1;
                        }




                    },
                    Err(e) => {
                    eprintln!("Parsing Error on line {}: {}", line_number, e[0]);
                    exit(1);
                    },
                }



            }
            for rule in input {
                println!("{}", rule.to_string_unked(unk.threshold, count_map.clone()));
            }
        },
        Some(Commands::Debinarise) => {
            let stdin = io::stdin();
            let mut line_number = 0;
            for line in stdin.lock().lines() {
                line_number += 1;
                let example = line.unwrap();





                let parsed_ptb = ptb_parser().parse(example);

                match parsed_ptb {
                    Ok(rule_tree) => {
                        let debinarised_rule = debinarise(rule_tree);
                        println!("{}", debinarised_rule);
                        },
                    Err(e) => {
                        eprintln!("Parsing Error on line {}: {}", line_number, e[0]);
                        exit(1);
                        },
                }
            }
        },
        Some(Commands::Binarise(binarise_data)) => {
            let horizontal = 999;
            if let Some(horizontal) = binarise_data.horizontal {}
            let vertical = 1;
            if let Some(vertical) = binarise_data.vertical {}


            let stdin = io::stdin();
            let mut line_number = 0;
            for line in stdin.lock().lines() {
                line_number += 1;
                let example = line.unwrap();





                let parsed_ptb = ptb_parser().parse(example);

                match parsed_ptb {
                    Ok(rule_tree) => {
                        let binarised_rule = binarise(rule_tree, horizontal, vertical);
                        println!("{}", binarised_rule);
                        },
                    Err(e) => {
                        eprintln!("Parsing Error on line {}: {}", line_number, e[0]);
                        exit(1);
                        },
                }
            }
            

        },
        _ => {exit(22);}
    }




}

fn binarise(rule: Rule, horizontal: u32, vertical: u32) -> Rule {
    match rule{
        Rule::Lexical {lhs: _, rhs: _} => {
            rule
        },
        Rule::NonLexical {lhs, rhs} => {
            if rhs.len() <= 2 {
                Rule::NonLexical {lhs, rhs: rhs.into_iter().map(|subrule| binarise(subrule, horizontal, vertical)).collect()}
            } else {
                let mut old_lhs =lhs.clone();
                let parsed_lhs = parse_node(lhs.clone());
                match parsed_lhs {
                    Ok(node) => {
                        match node {
                            Node::Binarised{lhs: left, rhs:_} => {
                                old_lhs = left.clone();
                            },
                            _=>{},

                        }
                    },
                    _ => {},
                }
                if let Ok(Node::Binarised{lhs: old_lhs, rhs: _}) = parse_node(lhs.clone()) {}
                let new_lhs = format!("{}|<{}>", old_lhs.clone(), rhs.clone().into_iter().skip(1).rev().take(horizontal.try_into().unwrap()).rev()
                                                    .map(|subrule| subrule.get_lhs().clone()).collect::<Vec<String>>().join(","));
                Rule::NonLexical {lhs, rhs: rhs.clone().into_iter().take(1)
                                                .chain(rhs.clone().into_iter().skip(1).rev().skip(horizontal.try_into().unwrap()).rev())
                                                .chain(iter::repeat(Rule::NonLexical {   lhs: new_lhs, 
                                                                                        rhs: rhs.clone().into_iter().skip(1).rev().take(horizontal.try_into().unwrap()).rev().collect()}).take(1)
                                                )
                                                .map(|subrule| binarise(subrule, horizontal, vertical)).collect()}
                
            }
        }
    }
}
 
fn debinarise(rule: Rule) -> Rule{
    match rule {
        Rule::Lexical {ref lhs, ref rhs} => {
            rule
        }
        Rule::NonLexical {ref lhs, ref rhs} => {
            let node_result = parse_node(rhs.last().unwrap().get_lhs().to_string());
            if let Ok(node) = node_result {
                match node {
                    Node::Debinarised {lhs: node_lhs} => {
                        Rule::NonLexical { lhs: lhs.to_string(), rhs: rhs.into_iter().map(|r| debinarise(r.clone())).collect()}
                    },
                    Node::Binarised {lhs: node_lhs, rhs: node_rhs} => {
                        match rhs.last().unwrap() {
                            Rule::Lexical {lhs: _, rhs: _} => {
                                rule //should be an error
                            },
                            Rule::NonLexical {lhs: _, rhs: new_children} => {
                                let mut mut_new_children = new_children.clone();
                                let new_rhs: Vec<Rule> = rhs.split_last().unwrap().1.iter().cloned().chain(mut_new_children.iter().cloned()).collect();
                                debinarise(Rule::NonLexical { lhs: lhs.to_string(), rhs: new_rhs})
                            }
                        }
                        
                        
                    }
                }
            } else {
                rule
            }  
        }    
    }
}

fn parse_node(input: String) -> Result<Node, Vec<Simple<char>>>{
    node_parser().parse(input)
}

fn node_parser() -> impl chumskyparser<char, Node, Error = Simple<char>>{
    let text = one_of("|")
        .not()
        .repeated()
        .at_least(1)
        .collect::<String>();

    let no_comma = one_of("|<>,")
    .not()
    .repeated()
    .at_least(1)
    .or(just(',')
        .repeated()
        .exactly(1))
    .collect::<String>();


    let debinarised = text.clone()
    .then_ignore(end())
    .map(|lhs| Node::Debinarised {lhs});

    let binarised = text.clone()
        .then_ignore(just('|'))
        .then_ignore(just('<'))
        .then(no_comma.clone().separated_by(just(',')))
        .then_ignore(just('>'))
        .map(|(lhs, rhs)| Node::Binarised {
        lhs,
        rhs,
        });

    debinarised.or(binarised)
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
enum Node {
    Binarised{lhs: String, rhs: Vec<String>},
    Debinarised{lhs: String},
}


fn parse_sentence(sentence: String) -> Result<Vec<String>, Vec<Simple<char>>>{
    one_of(" ")
        .not()
        .repeated()
        .at_least(1)
        .collect::<String>()
        .separated_by(just(' '))
        .parse(sentence)
}

// parser for the penn tree bank format in chumsky
fn ptb_parser() -> impl chumskyparser<char, Rule, Error = Simple<char>>{

    let text = one_of("() ")
        .not()
        .repeated()
        .at_least(1)
        .collect::<String>();

    let rule = recursive(|rule| {
        let lexical = text.clone()
        .then_ignore(just(' '))
        .then(text.clone())
        .delimited_by(just('('), just(')'))
        .map(|(lhs, rhs)| Rule::Lexical {
        lhs,
        rhs,
        });

        let nonlexical = text.clone()
            .then_ignore(just(' '))
            .then(rule
                .then_ignore(just(' ').or_not())
                .repeated())
            .delimited_by(just('('), just(')'))
            .map(|(lhs, rhs)| Rule::NonLexical {
            lhs,
            rhs,
            });
        lexical.or(nonlexical)
    });
    rule
}

impl fmt::Display for Rule {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Rule::Lexical {lhs, rhs} => write!(f, "({lhs} {rhs})"),
            Rule::NonLexical {lhs, rhs} => {
                let all_rhs: String = rhs.iter().map(std::string::ToString::to_string).collect::<Vec<String>>().join(" ");
                write!(f, "({lhs} {all_rhs})")
            }
        }
    }
}




fn readoff_rule(rule: &Rule) -> (Vec<(String,String)>,Vec<(String,String)>,Vec<String>) {
    let mut lexicon: Vec<(String,String)> = vec![];
    let mut rules: Vec<(String,String)> = vec![];
    let mut words: Vec<String> = vec![];

    match rule {
        Rule::Lexical {lhs, rhs} => {
            words.push(String::from(rhs));
            lexicon.push((String::from(lhs), String::from(rhs)));
        }
        Rule::NonLexical {lhs, rhs} => {
            let children: String = rhs
                        .iter().map(Rule::get_lhs)
                        .map(String::as_str)
                        .collect::<Vec<&str>>()
                        .join(" ");
            rules.push((String::from(lhs), children));

            for r in rhs {
            let (rules2, lexicon2, words2);
            (rules2, lexicon2, words2) = readoff_rule(r);
            rules.extend(rules2);
            lexicon.extend(lexicon2);
            words.extend(words2);
            }

        }
    }

    (rules, lexicon, words)
}

impl Rule {



    fn get_lhs(&self) -> &String {
        match self {
            Rule::Lexical {lhs, rhs: _} => lhs,
            Rule::NonLexical {lhs, rhs: _} => lhs,
        }
    }

    fn to_string_unked(&self, threshold: u32, count_map: HashMap<String, u32>) -> String {

        match self {
            Rule::Lexical {lhs, rhs} => {
                if count_map.get(rhs).unwrap() <= &threshold {

                    format!("({} UNK)", lhs)
                } else {

                
                    format!("({} {})", lhs, rhs)
                }
            }
            Rule::NonLexical {lhs, rhs} => {
                let all_rhs: String = rhs.iter().map(|s| s.to_string_unked(threshold, count_map.clone())).collect::<Vec<String>>().join(" ");
                format!("({} {})", lhs, all_rhs)
            }
        }
    }
}




// parsing from here, ill figure out modules or whatever later
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
enum ParseRule {
    Nary{lhs: String, rhs: Vec<String>, weight: Ratio<BigInt>},
    Unary{lhs: String, rhs: String, weight: Ratio<BigInt>},
}

impl ParseRule {
    fn get_lhs(&self) -> &String {
        match self {
            ParseRule::Nary {lhs, rhs: _, weight: _} => lhs,
            ParseRule::Unary {lhs, rhs: _, weight: _} => lhs,
        }
    }
}


#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
struct ParseLexical {
    lhs: String,
    rhs: String,
    weight: Ratio<BigInt>,
}

fn weightedCYK(LexicalRules: BTreeSet<ParseLexical>, NonLexicalRules: BTreeSet<ParseRule>, Sentence: Vec<String>) -> HashMap<(usize, usize, String), (Ratio<BigInt>, Rule)> {
    let mut WeightMap: HashMap<(usize, usize, String), (Ratio<BigInt>, Rule)> = HashMap::new();
    let mut NonTerminals: BTreeSet<String> = BTreeSet::new();
    let SentenceLength = Sentence.len();


    for rule in &NonLexicalRules {
        let NonTerminal = ParseRule::get_lhs(rule).to_string();

        for i in 0..=SentenceLength {

            for j in i+1..=SentenceLength {
                NonTerminals.insert(NonTerminal.clone());
                WeightMap.insert((i,j,NonTerminal.clone()),(Ratio::from_integer(BigInt::from(0)),Rule::Lexical{lhs:String::new(),rhs:String::new()}));
            }
        }


    }
    for rule in &LexicalRules {
        let NonTerminal = &rule.lhs;

        for i in 0..=SentenceLength {
            for j in i+1..=SentenceLength {

                NonTerminals.insert(NonTerminal.to_string());
                WeightMap.insert((i,j,NonTerminal.clone()),(Ratio::from_integer(BigInt::from(0)),Rule::Lexical{lhs:String::new(),rhs:String::new()}));
            }
        }


    }


    for i in 1..=SentenceLength {
        for rule in &LexicalRules {
            let (old_weight,_backtrace) = &WeightMap[&(i-1,i,rule.lhs.clone())];
            let new_weight = rule.weight.clone();
            if (new_weight > old_weight.clone()) & (Sentence[i-1] == rule.rhs) {
                WeightMap.insert((i-1,i,rule.lhs.clone()), (new_weight, Rule::Lexical{lhs: rule.lhs.clone(),rhs: rule.rhs.clone()}));
            }


        }
        let mut LocalWeightMap: HashMap<String, (Ratio<BigInt>, Rule)> = HashMap::new();
        for NonTerminal in &NonTerminals {
            LocalWeightMap.insert(NonTerminal.clone(), WeightMap.get(&(i-1,i,NonTerminal.clone())).unwrap().clone());
        }
        let NewLocalWeightMap: HashMap<String, (Ratio<BigInt>, Rule)> = unary_closure(LocalWeightMap.clone(), NonLexicalRules.clone(),NonTerminals.clone());

        for NonTerminal in &NonTerminals {
        // calculate unary  closure for  i-1,i from i-1,i and &LexicalRules and weights
            WeightMap.insert((i-1,i,NonTerminal.clone()), NewLocalWeightMap.get(NonTerminal).unwrap().clone());
        }

    }

    for r in 2..=SentenceLength {
        for i in 0..=SentenceLength-r {
            let j = i + r;
            for NonTerminal in &NonTerminals {
                for m in (i + 1)..j {
                    for rule in &NonLexicalRules {
                        match rule {
                            ParseRule::Nary {lhs, rhs, weight} => {
                                if lhs != NonTerminal {
                                    continue;
                                }
                                let (old_weight, _backtrace) = &WeightMap[&(i,j,NonTerminal.clone())];
                                let (left_hand_weight, left_hand_backtrace) = &WeightMap[&(i,m,rhs[0].clone())];
                                let (right_hand_weight, right_hand_backtrace) = &WeightMap[&(m,j,rhs[1].clone())];
                                let new_weight = weight * left_hand_weight * right_hand_weight;


                                if new_weight > old_weight.clone() {
                                    WeightMap.insert((i,j,NonTerminal.clone()),
                                        (new_weight,Rule::NonLexical{lhs: lhs.clone(), rhs: vec![left_hand_backtrace.clone(),right_hand_backtrace.clone()]}));
                                }
                            },
                            _ => {},
                        }
                    }
                }
            }

            let mut LocalWeightMap: HashMap<String, (Ratio<BigInt>, Rule)> = HashMap::new();
            for NonTerminal in &NonTerminals {
                LocalWeightMap.insert(NonTerminal.clone(), WeightMap.get(&(i,j,NonTerminal.clone())).unwrap().clone());
            }
            let NewLocalWeightMap: HashMap<String, (Ratio<BigInt>, Rule)> = unary_closure(LocalWeightMap.clone(), NonLexicalRules.clone(),NonTerminals.clone());

            for NonTerminal in &NonTerminals {

                WeightMap.insert((i,j,NonTerminal.clone()), NewLocalWeightMap.get(NonTerminal).unwrap().clone());
            }


        }
    }
    WeightMap

}


#[derive(PartialEq, Eq, Hash, Debug)]
struct queue_entry {
    NonTerminal: String,
    rule: Rule,
}

fn unary_closure(mut WeightMap: HashMap<String, (Ratio<BigInt>, Rule)>, NonLexicalRules: BTreeSet<ParseRule>, NonTerminals: BTreeSet<String>) -> HashMap<String, (Ratio<BigInt>, Rule)> {
    let mut queue = PriorityQueue::new();
    // : Queue<((Ratio<BigInt>, Rule), String)>
    //println!("{:?} \n", WeightMap);

    for NonTerminal in &NonTerminals {
        let (weight, rule) = WeightMap.get(NonTerminal).unwrap().clone();
        if weight > Ratio::from_integer(BigInt::from(0)) {
            queue.push(queue_entry {NonTerminal: NonTerminal.to_string(), rule: rule.clone(),}, weight);
        }
        WeightMap.insert(NonTerminal.to_string(), (Ratio::from_integer(BigInt::from(0)), rule.clone()));
    }

    while !queue.is_empty() {
        let (maximum, max_weight) = queue.pop().unwrap();
        let (old_weight, _old_rule) = WeightMap.get(&maximum.NonTerminal).unwrap().clone();
        if max_weight > old_weight {
            WeightMap.insert(maximum.NonTerminal.clone(), (max_weight.clone(), maximum.rule.clone()));
            //println!("maximum: {:?}, max_weight: {:?}", maximum, max_weight);


            for rule in &NonLexicalRules {
                match rule {
                    ParseRule::Unary {lhs, rhs, weight} => {


                        if rhs == &maximum.NonTerminal {
                            let entry = queue_entry {NonTerminal: lhs.to_string(),
                                                rule: Rule::NonLexical{lhs: lhs.clone(), rhs: vec![maximum.rule.clone()]},};
                            let new_weight = max_weight.clone()*weight;
                            //println!("{:?}", entry);
                            //println!("{:?}", new_weight);

                            queue.push(entry , new_weight);
                        }
                    },
                    _ => {},
                }
            }


        }
    }
    //println!("{:?} \n", WeightMap);
    WeightMap
}



fn FloatStringToRatio(s: String) -> Ratio<BigInt> {
    let float: f64 = s.parse().unwrap();
    Ratio::from_float(float).unwrap()
}

fn lexical_rule_parser() -> impl chumskyparser<char, ParseLexical, Error = Simple<char>>{
    let text = one_of(' ')
        .not()
        .repeated()
        .at_least(1)
        .collect::<String>();

    text.clone()
    .then_ignore(just(' '))
    .then(text.clone())
    .then_ignore(just(' '))
    .then(text)
    .then_ignore(end())
    .map(|((lhs, rhs), number)| ParseLexical {
    lhs,
    rhs,
    weight: FloatStringToRatio(number),
    })
}

fn rules_list_parser() -> impl chumskyparser<char, ParseRule, Error = Simple<char>>{

    let text = one_of(" ")
        .not()
        .repeated()
        .at_least(1)
        .collect::<String>();


    let unary = text.clone()
        .then_ignore(just(" -> "))
        .then(text.clone())
        .then_ignore(just(' '))
        .then(text.clone())
        .then_ignore(end())
        .map(|((lhs, rhs), number)| ParseRule::Unary {
        lhs,
        rhs,
        weight: FloatStringToRatio(number),
        });

    let nary = text.clone()
        .then_ignore(just(" -> "))
        .then(text.clone()
            .then_ignore(just(' '))
            .repeated())
        .then(text.clone())
        .map(|((lhs, rhs), number)| ParseRule::Nary {
        lhs,
        rhs,
        weight: FloatStringToRatio(number),
        });

    unary.or(nary)
}

















#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use super::Rule::{Lexical, NonLexical};
    use super::Node::{Debinarised, Binarised};



    #[test]
    fn test_ptb_parser() {
        let input = "(S (N John) (VP (V hit) (NP (D the) (N ball))))";
        let parsed_ptb = ptb_parser().parse(input);
        let output = Ok(NonLexical { lhs: "S".to_string(),
                                     rhs: vec![Lexical { lhs: "N".to_string(),
                                                     rhs: "John".to_string() },
                                               NonLexical { lhs: "VP".to_string(),
                                                            rhs: vec![Lexical { lhs: "V".to_string(), rhs: "hit".to_string() },
                                                                      NonLexical { lhs: "NP".to_string(), rhs: vec![Lexical { lhs: "D".to_string(), rhs: "the".to_string() },
                                                                                                                    Lexical { lhs: "N".to_string(), rhs: "ball".to_string() }
                                                                                                                   ] }
                                                                     ] }
                                              ]
                                    });
        assert_eq!(parsed_ptb, output)
    }

    #[test]
    fn test_ptb_parser_lexical_multiple() {
    let parsed_ptb = ptb_parser().parse("(S word1 word2)");
    assert!(parsed_ptb.is_err())
    }

    #[test]
    fn test_ptb_parser_lexical_multiple_nested() {
    let parsed_ptb = ptb_parser().parse("(S (VP word1 word2))");
    assert!(parsed_ptb.is_err())
    }

    #[test]
    fn test_ptb_parser_no_rule() {
    let parsed_ptb = ptb_parser().parse("(string)");
    assert!(parsed_ptb.is_err())
    }

    #[test]
    fn test_ptb_parser_no_parantheses() {
    let parsed_ptb = ptb_parser().parse("S word");

    assert!(parsed_ptb.is_err())
    }

    #[test]
    fn test_ptb_parser_open_parantheses() {
    let parsed_ptb = ptb_parser().parse("(S word");

    assert!(parsed_ptb.is_err())
    }

    #[test]
    fn test_parse_sentence_without_period() {
        let input = "Paul likes Peter".to_string();
        let output = Ok(vec!["Paul".to_string(), "likes".to_string(), "Peter".to_string()]);
        assert_eq!(parse_sentence(input), output)
    }

    #[test]
    fn test_parse_sentence_with_period() {
        let input = "Paul likes Peter .".to_string();
        let output = Ok(vec!["Paul".to_string(), "likes".to_string(), "Peter".to_string(), ".".to_string()]);
        assert_eq!(parse_sentence(input), output)
    }

    #[test]
    fn test_parse_node_binarised() {
        let input = "S|<B,C,D>".to_string();
        let output = Ok(Binarised {   lhs: "S".to_string(),
                                        rhs: vec!["B".to_string(), "C".to_string(), "D".to_string()] ,
                                    });
        assert_eq!(parse_node(input), output);
    }

    #[test]
    fn test_parse_node_binarised_with_comma() {
        let input = "NAC-LOC|<NNP,,>".to_string();
        let output = Ok(Binarised {   lhs: "NAC-LOC".to_string(),
                                        rhs: vec!["NNP".to_string(), ",".to_string()] ,
                                    });
        assert_eq!(parse_node(input), output);
    }

    #[test]
    fn test_parse_node_debinarised() {
        let input = ",".to_string();
        let output = Ok(Debinarised { lhs: input.clone()});
        assert_eq!(parse_node(input), output);

        let input2 = "Banane".to_string();
        let output2 = Ok(Debinarised { lhs: input2.clone()});
        assert_eq!(parse_node(input2), output2);

        let input3 = "D".to_string();
        let output3 = Ok(Debinarised { lhs: input3.clone()});
        assert_eq!(parse_node(input3), output3);
    }

    #[test]
    fn test_debinarise_rule() {
        let input = ptb_parser().parse("(S (A a) (S|<B,C,D> (B b) (S|<C,D> (C c) (D d))))".to_string());
        let output = ptb_parser().parse("(S (A a) (B b) (C c) (D d))".to_string());
        if let Ok(i) = input {
            if let Ok(o) = output {
                assert_eq!(debinarise(i), o);
            }
        }
         
        
    }

    #[test]
    fn test_binarise_rule() {
        let output = ptb_parser().parse("(S (A a) (S|<B,C,D> (B b) (S|<C,D> (C c) (D d))))".to_string());
        let input = ptb_parser().parse("(S (A a) (B b) (C c) (D d))".to_string());
        if let Ok(i) = input {
            if let Ok(o) = output {
                assert_eq!(binarise(i, 999, 1), o);
            }
        }
         
        
    }

    #[test]
    fn test_debinarise_gold_rule() {
        let input = ptb_parser().parse("(ROOT (S (NP-SBJ (NP (NNP Pierre) (NNP Vinken)) (NP-SBJ|<,,ADJP,,> (, ,) (NP-SBJ|<ADJP,,> (ADJP (NP (CD 61) (NNS years)) (JJ old)) (, ,)))) (S|<VP,.> (VP (MD will) (VP (VB join) (VP|<NP,PP-CLR,NP-TMP> (NP (DT the) (NN board)) (VP|<PP-CLR,NP-TMP> (PP-CLR (IN as) (NP (DT a) (NP|<JJ,NN> (JJ nonexecutive) (NN director)))) (NP-TMP (NNP Nov.) (CD 29)))))) (. .))))
        ".to_string());
        let output = ptb_parser().parse("(ROOT (S (NP-SBJ (NP (NNP Pierre) (NNP Vinken)) (, ,) (ADJP (NP (CD 61) (NNS years)) (JJ old)) (, ,)) (VP (MD will) (VP (VB join) (NP (DT the) (NN board)) (PP-CLR (IN as) (NP (DT a) (JJ nonexecutive) (NN director))) (NP-TMP (NNP Nov.) (CD 29)))) (. .)))".to_string());
        if let Ok(i) = input {
            if let Ok(o) = output {
                assert_eq!(debinarise(i), o);
            }
        }
         
        
    }

    #[test]
    fn test_binarise_gold_rule() {
        let output = ptb_parser().parse("(ROOT (S (NP-SBJ (NP (NNP Pierre) (NNP Vinken)) (NP-SBJ|<,,ADJP,,> (, ,) (NP-SBJ|<ADJP,,> (ADJP (NP (CD 61) (NNS years)) (JJ old)) (, ,)))) (S|<VP,.> (VP (MD will) (VP (VB join) (VP|<NP,PP-CLR,NP-TMP> (NP (DT the) (NN board)) (VP|<PP-CLR,NP-TMP> (PP-CLR (IN as) (NP (DT a) (NP|<JJ,NN> (JJ nonexecutive) (NN director)))) (NP-TMP (NNP Nov.) (CD 29)))))) (. .))))
        ".to_string());
        let input = ptb_parser().parse("(ROOT (S (NP-SBJ (NP (NNP Pierre) (NNP Vinken)) (, ,) (ADJP (NP (CD 61) (NNS years)) (JJ old)) (, ,)) (VP (MD will) (VP (VB join) (NP (DT the) (NN board)) (PP-CLR (IN as) (NP (DT a) (JJ nonexecutive) (NN director))) (NP-TMP (NNP Nov.) (CD 29)))) (. .)))".to_string());
        if let Ok(i) = input {
            if let Ok(o) = output {
                assert_eq!(binarise(i, 999, 1), o);
            }
        }
         
        
    }
}
